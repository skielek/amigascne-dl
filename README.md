# amigascne-dl

This is a lightweight command line utility for querying and downloading files from the legendary amigascne.org archive (and modland!). It should run on any modern *nix OS, but has only been tested on Linux and MacOS.

For minimum functionality (e.g. querying the web service) you need nothing more than a **bash** shell. However to actually download any files, it requires either **curl** or **wget** are installed. One or both of these are installed on more or less every modern Linux and Mac OS install by default.

## Installation

Download and place amigascne-dl wherever you like, typically in your $PATH. After downloading don't forget to make it executable:

``chmod +x amigascne-dl``

## Usage

In the most basic use-case just run the script with the only argument passed being whatever you're searching for. If you do not provide an argument or if you pass the `-h` option, you'll get the usage statement, which shows the available options.

~~~
Usage: amigascne-dl [options...] <search string>
 -g <group name>		Limit search to group name
 -m				Search modland archive
~~~

Downloaded files are stored in the users **$HOME/Downloads** folder.

## Examples

If you wanted to find the demo **StateOfTheArt**, then you might do something like this: 
![alt text](screenshots/Screenshot1.png "Example 1")

Now depending on your search string, you may get a lot of results back that are unrelated to what you're looking for. In which case you'll want to make your search more specific. One easy way to do that is by specifying the group. So this time let's use the `-g` option to specifiy the group **Spaceballs** along with our original query for **StateOfTheArt**:
![alt text](screenshots/Screenshot2.png "Example 2")

You can also use *regex* queries to get even more granular results. So let's now kill two birds with one stone and show both the `-m` option to search the **modland** archive whilst using *regex* to find all releases of the **Protracker** application in an **LHA** archive:
![alt text](screenshots/Screenshot3.png "Example 3")
